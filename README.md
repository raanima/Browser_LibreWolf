# Browser_LibreWolf
    Качаю "LibreWolf" от сюда: https://librewolf.net/installation/linux/ расширение "appimage"
    Перемещаю его по пути: /home/user/.local/share/applications/appimage/LibreWolf.x86_64.AppImage
    С этой директории где находится "AppImage" от крываем терминал, и даем разрешение на запуск.
    
    chmod +x ./LibreWolf.x86_64.AppImage
   
    А потом можем отсюда открыть если надо: ./LibreWolf.x86_64.AppImage
    
    Закидываю свою иконку по пути: /home/user/.local/share/applications/icons/Librewoof.svg
    И также файлик с расширением ".desktop" по пути: /home/user/.local/share/applications/LibreWolf.desktop
    Это все найдете сдесь.
    После того как два раза нажмете на исполняймый файл: подвердите, сделав его доверенным.
    Дале просто иконку с расширением ".desktop" перетаскивате как бы на понельку или "plank" и все, у вас 
    красивая иконка.

    Следущий шаг настройка браузера, вы будите настраивать его по своему усматрению, 
    практически с такими-же расширениями. Которые вы можете скачать с магазина.
    
    А нам нужно сделать вот что:
    Заходим в настройки, ставим галочку на "Open previous windows and tabs"  : https://postimg.cc/232rtHbP

    Но она пока что ни работает, нам нужно сейчас снять галочку.
    Заходим в Приватность и Безопасность, и снимаем галочку с "Clear history when LibreWolf closes" :      
    https://postimg.cc/7bWY6sHS

    Это для того что бы при закрытие браузера открытые вкладки остались.